import PIL.Image as Image
import random
import math

def output3():
    baseim = Image.open( "src/base_1.png" )
    # outim = Image.open( "src/fileout_base.png" )
    # outim=Image.new('RGBA',baseim.size,(255,255,255,255))
    output_size = 2048
    outim = Image.new('RGBA',(output_size, output_size),(255,255,255,0))
    pix = baseim.load()
    pix_pic_size = 20
    scan_num = int(math.floor(output_size/pix_pic_size))
    scan_dis = baseim.size[0]/float(scan_num)
    shift_dis = 4
    for x_ind in range(scan_num):
        for y_ind in range(scan_num):
            x = int(math.floor(x_ind*scan_dis))
            y = int(math.floor(y_ind*scan_dis))
            print x, y
            if pix[x,y][0] == 237 and pix[x,y][1] == 28 and pix[x,y][2] == 36:
                target_image_name = "src/b.png"
            elif pix[x,y][0] == 255 and pix[x,y][1] == 255 and pix[x,y][2] == 255:
                target_image_name = "src/00.png"
            elif pix[x,y][0] == 255 and pix[x,y][1] == 242 and pix[x,y][2] == 0:
                target_image_name = "src/01.png"
            elif pix[x,y][0] == 0 and pix[x,y][1] == 0 and pix[x,y][2] == 0:
                ran_num = random.random()*4
                if ran_num < 1:
                    target_image_name = "src/f1.png"
                elif ran_num < 2:
                    target_image_name = "src/f2.png"
                elif ran_num < 3:
                    target_image_name = "src/f3.png"
                else:
                    target_image_name = "src/02.png"
            elif pix[x,y][0] == 255 and pix[x,y][1] == 127 and pix[x,y][2] == 39:
                target_image_name = "src/08.png"
            elif pix[x,y][0] == 0 and pix[x,y][1] == 162 and pix[x,y][2] == 232:
                target_image_name = "src/07.png"
            elif pix[x,y][0] == 181 and pix[x,y][1] == 230 and pix[x,y][2] == 29:
                target_image_name = "src/05.png"
            ran_num = random.random()
            floatim = Image.open( target_image_name )
            floatim.thumbnail( (pix_pic_size,pix_pic_size) )
            floatim = floatim.rotate( ran_num*90-45, Image.BILINEAR )
            outim.paste( floatim, (pix_pic_size*x_ind+shift_dis, pix_pic_size*y_ind+shift_dis), floatim)
    print scan_num
    print scan_dis
    outim.save( "src/fileout4.png" )

def output1():
    baseim = Image.open( "src/base_1.png" )
    # outim = Image.open( "src/fileout_base.png" )
    outim=Image.new('RGBA',baseim.size,(255,255,255,255))
    pix = baseim.load()
    for x in range(baseim.size[0]):
        for y in range(baseim.size[1]):
            if x % 24 == 0 and y % 24 == 0:
                print x, y
                if pix[x,y][0] == 237 and pix[x,y][1] == 28 and pix[x,y][2] == 36:
                    target_image_name = "src/b.png"
                elif pix[x,y][0] == 255 and pix[x,y][1] == 255 and pix[x,y][2] == 255:
                    target_image_name = "src/00.png"
                elif pix[x,y][0] == 255 and pix[x,y][1] == 242 and pix[x,y][2] == 0:
                    target_image_name = "src/01.png"
                elif pix[x,y][0] == 0 and pix[x,y][1] == 0 and pix[x,y][2] == 0:
                    ran_num = random.random()*4
                    if ran_num < 1:
                        target_image_name = "src/f1.png"
                    elif ran_num < 2:
                        target_image_name = "src/f2.png"
                    elif ran_num < 3:
                        target_image_name = "src/f3.png"
                    else:
                        target_image_name = "src/02.png"
                elif pix[x,y][0] == 255 and pix[x,y][1] == 127 and pix[x,y][2] == 39:
                    target_image_name = "src/08.png"
                elif pix[x,y][0] == 0 and pix[x,y][1] == 162 and pix[x,y][2] == 232:
                    target_image_name = "src/07.png"
                elif pix[x,y][0] == 181 and pix[x,y][1] == 230 and pix[x,y][2] == 29:
                    target_image_name = "src/05.png"
                ran_num = random.random()
                floatim = Image.open( target_image_name )
                floatim.thumbnail( (24,24) )
                floatim = floatim.rotate( ran_num*90-45, Image.BILINEAR )
                outim.paste( floatim, (x, y), floatim)
    outim.save( "src/fileout3.png" )

def output2():
    baseim = Image.open( "src/fileout_base.png" )
    outim = Image.open( "src/fileout_base.png" )
    pix = baseim.load()
    for x in range(baseim.size[0]):
        for y in range(baseim.size[1]):
            if x % 24 == 0 and y % 24 == 0:
                print x, y
                if abs(pix[x,y][0] - pix[x,y][1]) < 20 and abs(pix[x,y][1] - pix[x,y][2]) < 20 and pix[x,y][0] < 200:
                    target_image_name = "src/06.png"    ## black
                elif pix[x,y][0] > 200 and pix[x,y][0] - pix[x,y][1] > 30 and pix[x,y][0] - pix[x,y][2] > 20:
                    target_image_name = "src/01.png"    ## red
                elif pix[x,y][2] - pix[x,y][0] > 60 and pix[x,y][2] - pix[x,y][1] > 30 and pix[x,y][2] > 150:
                    target_image_name = "src/02.png"    ## blue
                elif abs(pix[x,y][0] - pix[x,y][1])<30 and abs(pix[x,y][0] - pix[x,y][2])>30:
                    if(pix[x,y][0] > 200):
                        target_image_name = "src/04.png"    ## y1
                    else:
                        target_image_name = "src/05.png"    ## y2
                elif pix[x,y][1] > 200 and pix[x,y][1] - pix[x,y][0] > 15 and pix[x,y][1] - pix[x,y][2] > 15:
                    target_image_name = "src/07.png"    ## green
                else:
                # elif abs(pix[x,y][0] - pix[x,y][1]) < 15 and abs(pix[x,y][1] - pix[x,y][2]) < 15 and pix[x,y][0] <= 150:
                    target_image_name = "src/00.png"    ## white
                ran_num = random.random()
                floatim = Image.open( target_image_name )
                floatim.thumbnail( (24,24) )
                floatim = floatim.rotate( ran_num*90-45, Image.BILINEAR )
                outim.paste( floatim, (x, y), floatim)
    outim.save( "src/fileout1.png" )

if __name__ == '__main__':
    # im = Image.open( "src/00.png" )
    # print im.format, im.size, im.mode

    # im = im.rotate( 45, Image.BILINEAR )
    # # im = im.transpose( Image.ROTATE_90 )

    output3()