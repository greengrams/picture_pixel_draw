# 透明背景網頁預覽
[click](https://greengrams.gitlab.io/picture_pixel_draw/main.html)  
***
# Result
[background Akari picture](https://greengrams.gitlab.io/picture_pixel_draw/src/fileout.png)  
[fit background picture color](https://greengrams.gitlab.io/picture_pixel_draw/src/fileout1.png)  
[2400*2400 PNG white background](https://greengrams.gitlab.io/picture_pixel_draw/src/fileout3.png)  
[2048*2048 PNG transparent background](https://greengrams.gitlab.io/picture_pixel_draw/src/fileout4.png)  
***
# 材料來源
[Mirai Akari Project](https://www.youtube.com/channel/UCMYtONm441rBogWK_xPH9HA)